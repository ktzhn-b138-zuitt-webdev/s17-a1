// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
let students = [];

function addStudent(add){
	students.push(add);
	console.log(add + " was added to the student's list.");
}

// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.");

}

// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){
	students.sort();
	students.forEach(function(student){
		console.log(student);
	});	
}

/*6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.
*/


function findStudent(name){
	let filteredStudents = students.filter(function(result){
		return result.toLowerCase().includes(name);	
	})

	if (filteredStudents.length == 1 ){
		console.log(filteredStudents[0] + ' is an enrollee.'); 
	}
	else if(filteredStudents.length >= 2){
		console.log(filteredStudents.toString() + ' are enrollees'); 
	}
	else if(filteredStudents.length == 0){
		console.log(name + ' is not an enrollee.'); 
	}


}

// removeStudent function? Based on given image of expected output

function removeStudent(name){
	let studentIndex = students.indexOf(name);
	students.splice(studentIndex, (studentIndex+1));
	console.log(name + " was removed from the student's list.");
}